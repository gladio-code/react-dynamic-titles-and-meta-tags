import React from "react";
import { Helmet } from "react-helmet";

export const Contact = () => {
  return (
    <div>
      <Helmet>
        <title>This is the contact page</title>
        <meta
          name="description"
          content="This is my meta description of the contact page"
        />
      </Helmet>
      <h1>Contact</h1>
    </div>
  );
};
