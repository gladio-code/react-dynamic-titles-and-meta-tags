import React from "react";
import { Helmet } from "react-helmet";

export const Homepage = () => {
  return (
    <div>
      <Helmet>
        <title>Home page!</title>
        <meta name="description" content="This is my meta description" />
      </Helmet>
      <h1>Homepage</h1>
    </div>
  );
};
